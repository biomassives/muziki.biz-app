Buy Mziki
============

The Buy Mziki app powered by Meteor and makes use of the LocalMarket demo via [Percolate Studio](http://percolatestudio.com) for its core concepts and data model guidance

 In this app uses:

  - sample database schema to generate lists and items, page views- easy to maintain and add to.
  - Integrating OAUTH with Meteor's accounts-ui package - facebook, twiiter, google, etc.
  - Cordova integration to use device phone and GPS - for iphone
  - Mobile UI & UX  - :)